

public class Hydro extends bill implements IDisplay {
	String agencyName;
	double unitsConsumed;

	public Hydro(int billId, String billDate, String billType, double totalbillAmount, String agencyName,
			double unitConsumed) {
		super(billId, billDate, billType, totalbillAmount);
		
		this.agencyName = agencyName;
		this.unitsConsumed = unitConsumed;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public double getUnitConsumed() {
		return unitsConsumed;
	}

	public void setUnitConsumed(double unitConsumed) {
		this.unitsConsumed = unitConsumed;
	}

	@Override
	public void Display() {
		// TODO Auto-generated method stub
		super.Display();
		System.out.println("Bill Id: " + this.billId+"\n " + " Bill Date: " + this.billDate+"\n " + " Bill Type: " + this.billType+"\n "
				+ " Bill Amount: $" + this.totalbillAmount+"\n " + " Company Name: " + this.agencyName +"\n "+ " Units Consumed: "
				+ this.unitsConsumed + "units");

	}
}
