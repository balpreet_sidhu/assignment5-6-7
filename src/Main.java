

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Customers c1 = new Customers(1, "Balpreet", "Sidhu", "Balpreet Sidhu", "balpreet.sidhu0011@gmail.com");
		bill b1 = new Hydro(1, "Wednesday, 19 June, 2019", "Hydro", 25.35, "HydroOne", 29);
		c1.billList(b1);
		bill b2 = new Internet(2, "Wednesday, 19 June, 2019", "Internet", 66.50, "Rogers", 500);
		c1.billList(b2);
		c1.Display();

		Customers c2 = new Customers(2, "Gunraj Bir", "Singh", "Gunraj Bir Singh", "gunrajbir14@gmail.com");
		bill b3 = new Hydro(1, "Wednesday, 19 June, 2019", "Hydro", 50.50, "Enbridge", 29);
		c2.billList(b3);
		bill b4 = new Internet(2, "Wednesday, 19 June, 2019", "Internet", 56.50, "Rogers", 500);
		c2.billList(b4);
		bill b5 = new Mobile(3, "Thursday, 24 January, 2019", "Mobile", 550.50, "Galaxy Samsung Inc.",
				"Prepaid Talk + Text plan", 12345678, 5, 356);
		c2.billList(b5);
		bill b6 = new Mobile(4, "Wednesday, 19 June, 2019", "Mobile", 500.50, "Apple Inc. iPhone X MAX+",
				"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
		c2.billList(b6);
		c2.Display();
		Customers c3 = new Customers(3, "Parminderjit ", "Singh", "Parminderjit Singh", "parminderdhiman110@gmail.com");
		bill b7 = new Mobile(4, "Wednesday, 19 June, 2019", "Mobile", 500.50, "Apple Inc. iPhone X MAX+",
				"LTE+3G 9.5GB Promo Plan", 5678947, 4, 230);
		c2.billList(b7);
	}

}
